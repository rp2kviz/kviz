<?php

class AdminController extends BaseController
{
  //pocetna stranica za admina-sluzi da ga posalje na login
  public function index()
  {
    $k=new Service();
    if(isset($_SESSION['admin']))
    {
      $this->registry->template->message="";
      $this->registry->template->title= 'Ime kviza';
      $this->registry->template->show('admin_novi_kviz');
      exit;
    }
    else {
      $this->registry->template->message="";
      $this->registry->template->title= 'Login';
      $this->registry->template->show('admin_login');
    }

  }

  public function login()
  {
    $this->registry->template->message="";

    //Ako je admin već ulogiran odmah odi na formu za novi kviz
    if(isset($_SESSION['admin']))
    {
      $this->registry->template->title= 'Ime kviza';
      $this->registry->template->show('admin_novi_kviz');
      exit;
    }

    //Provjera jesu li uneseni podatci za login
    $ks = new Service();
    if (!isset( $_POST['admin_username']) || !preg_match('/^[a-zA-Z ]+$/', $_POST['admin_username'])
            || !isset($_POST['admin_password']) ||!preg_match('/^[a-zA-Z0-9]+$/', $_POST['admin_password'])  )
            {
              $this->registry->template->title ="Neuspješan login";
              $this->registry->template->message="";
              $this->registry->template->show('admin_login');
              return;
            }

    $admini = $ks -> dohvati_admine();
    foreach ($admini as $admin)
    {
      if($admin->ime_admina === $_POST['admin_username']&& !password_verify($_POST['admin_password'], $admin->pass))
        {
          $this->registry->template->title ="Neuspješan login";
          $this->registry->template->message="Netočna lozinka!";
          $this->registry->template->show('admin_login');
          return;
        }
      if($admin->ime_admina === $_POST['admin_username']&& password_verify($_POST['admin_password'], $admin->pass))
        $_SESSION['admin'] =$admin->ime_admina;
    }
    if(!isset($_SESSION['admin']))
    {
      $this->registry->template->title="Login";
      $this->registry->template->message ="";
      $this->registry->template->show('admin_login');
      return;
    }

    else
    {
      $this->registry->template->title= 'Ime kviza';
      $this->registry->template->show('admin_novi_kviz');
    }
  }


  public function logout()
  {
    session_unset();
    session_destroy();
    header( 'Location: ' . __SITE_URL . '/index.php' );
  }


  public function kraj_kviza()
  {
    unset($_SESSION['ime_kviza']);
    unset($_SESSION['id_kviza']);
    unset($_SESSION['id_pitanja']);
    unset($_SESSION['broj_odgovora']);
    unset($_SESSION['broj_tocnih']);

    $this->registry->template->title="Stvori novi kviz";
    $this->registry->template->message ="Završeno stvaranje kviza";
    $this->registry->template->show('admin_novi_kviz');
  }


  public function stvaranje_kviza()
  {
    $this->registry->template->message="";
    $this->registry->template->title="Stvori novi kviz";

    $k=new Service();
    if(!isset($_SESSION['admin']))
    {
      $this->registry->template->message ="Niste ulogirani";
      header( 'Location: ' . __SITE_URL . '/index.php?rt=admin_login' );
      exit;
    }

    if(!strlen(trim($_POST['ime_kviza']))){
      $this->registry->template->message ="";
      $this->registry->template->show('admin_novi_kviz');
      exit;
    }

    $ime_kviza = $_POST['ime_kviza'];
    $slika = $_POST['slika'];
    $id_kviza = (integer)$k->max_id_kvizova();
    $id_kviza += 1;

    $_SESSION['ime_kviza'] = $ime_kviza;
    $_SESSION['id_kviza'] = $id_kviza;
    $_SESSION['br_pitanja'] = 1;

    $k->stvori_novi_kviz($id_kviza, $ime_kviza, $slika);
    $this->registry->template->br_pitanja = 1;
    $this->registry->template->naziv_kviza = $ime_kviza;
    $this->registry->template->title= 'Stvaranje novog pitanja';
    $this->registry->template->show('admin_novo_pitanje');

  }

  public function stvaranje_pitanja()
  {
    $this->registry->template->message="";
    $this->registry->template->title="Stvaranje pitanja";
    $this->registry->template->br_pitanja=$_SESSION['br_pitanja'];
    $this->registry->template->naziv_kviza = $_SESSION['ime_kviza'];

    if(!isset($_SESSION['admin']))
    {
      $this->registry->template->message ="Niste ulogirani";
      header( 'Location: ' . __SITE_URL . '/index.php?rt=admin_login' );
      exit;
    }

    if(!isset($_SESSION['ime_kviza']))
    {
      $this->registry->template->message="";
      $this->registry->template->show('admin_novi_kviz');
      exit;
    }

    if(!strlen(trim($_POST['tekst_pitanja'])))
    {
      $this->registry->template->message="Nije unesen tekst pitanja";
      $this->registry->template->show('admin_novo_pitanje');
      exit;
    }
    if(!isset($_POST['tip_odgovora']))
    {
      $this->registry->template->message="Nije odabran tip odgovora";
      $this->registry->template->show('admin_novo_pitanje');
      exit;
    }

    else
    {
      $tip_pitanja = (int)$_POST['tip_odgovora'];
      if($tip_pitanja===3)
      {
        $broj_odgovora = (int)$_POST['broj_odgovora_vise'];
        $broj_tocnih = (int)$_POST['broj_tocnih_odgovora'];
        if($broj_tocnih>$broj_odgovora)
          {
            $this->registry->template->message="Odabrano je više točnih od ponuđenih odgovora";
            $this->registry->template->show('admin_novo_pitanje');
            exit;
          }
        }
      }

      $k=new Service();
      $id_pitanja = (integer)$k->max_id_pitanja();
      $id_pitanja +=1;
      $id_kviza = $_SESSION['id_kviza'];
      $tekst_pitanja = $_POST['tekst_pitanja'];
      $redni_broj = $_SESSION['br_pitanja'];
      $slika_pitanja = $_POST['url_pitanja'];
      $_SESSION['id_pitanja'] = $id_pitanja;
      $_SESSION['tekst_pitanja'] = $tekst_pitanja;
      $_SESSION['tip_pitanja'] = $tip_pitanja;
      $k->stvori_novo_pitanje($id_pitanja, $id_kviza, $tip_pitanja, $tekst_pitanja, $redni_broj, $slika_pitanja);

      $redni_broj++;
      $_SESSION['br_pitanja'] = $redni_broj;
      $this->registry->template->br_pitanja=$redni_broj;
      $this->registry->template->pitanje=$tekst_pitanja;


      if($tip_pitanja===1)
      {
        $_SESSION['broj_odgovora']=1;
        $_SESSION['broj_tocnih']=1;
        $this->registry->template->title= 'Stvaranje odgovora';
        $this->registry->template->show('admin_odg_tekstualni');
      }
      elseif ($tip_pitanja===2)
      {
        $broj_odgovora = (int)$_POST['broj_odgovora_jedan'];
        $_SESSION['broj_odgovora'] = $broj_odgovora;
        $_SESSION['broj_tocnih']=1;
        $this->registry->template->br_odgovora=$broj_odgovora;
        $this->registry->template->title= 'Stvaranje odgovora';
        $this->registry->template->show('admin_odg_visestruki_jedan');
      }
      elseif($tip_pitanja===3)
      {
        $broj_odgovora = (int)$_POST['broj_odgovora_vise'];
        $broj_tocnih = (int)$_POST['broj_tocnih_odgovora'];
        $_SESSION['broj_odgovora'] = $broj_odgovora;
        $_SESSION['broj_tocnih']=$broj_tocnih;
        $this->registry->template->br_odgovora=$broj_odgovora;
        $this->registry->template->br_tocnih=$broj_tocnih;
        $this->registry->template->title= 'Stvaranje odgovora';
        $this->registry->template->show('admin_odg_visestruki_vise');
      }
    }


  public function stvaranje_odgovora()
  {
    $this->registry->template->message="";
    $this->registry->template->title="Stvaranje odgovora";
    $this->registry->template->pitanje=$_SESSION['tekst_pitanja'];
    $this->registry->template->br_pitanja=$_SESSION['br_pitanja'];
    $this->registry->template->naziv_kviza = $_SESSION['ime_kviza'];

    if(!isset($_SESSION['admin']))
    {
      $this->registry->template->message ="Niste ulogirani";
      header( 'Location: ' . __SITE_URL . '/index.php?rt=admin_login' );
      exit;
    }
    else
    {
      $k = new Service();
      //$id_odgovora, $id_pitanja, $tekst_odgovora, $tocno_netocno

      $broj_odgovora = $_SESSION['broj_odgovora'];
      $broj_tocnih = $_SESSION['broj_tocnih'];
      $id_pitanja = $_SESSION['id_pitanja'];

      $this->registry->template->br_odgovora=$broj_odgovora;
      $this->registry->template->br_tocnih=$broj_tocnih;

      $tocni = $_POST['tocan_odgovor'];

      foreach ($tocni as $tocan)
      {
        if(!strlen(trim($tocan)))
        {
          $this->registry->template->message="Nije unesen tekst svih točnih odgovora";
          if($_SESSION['tip_pitanja']===1)
              $this->registry->template->show('admin_odg_tekstualni');

          elseif ($_SESSION['tip_pitanja']===2)
              $this->registry->template->show('admin_odg_visestruki_jedan');

          else
              $this->registry->template->show('admin_odg_visestruki_vise');

          exit;
        }
      }

      if($broj_odgovora !== $broj_tocnih)
      {
          $netocni = $_POST['netocan_odgovor'];
          foreach ($netocni as $netocan)
          {
            if(!strlen(trim($netocan)))
            {
              $this->registry->template->message="Nije unesen tekst svih ponuđenih odgovora";
              if($_SESSION['tip_pitanja']===1)
                  $this->registry->template->show('admin_odg_tekstualni');

              elseif ($_SESSION['tip_pitanja']===2)
                  $this->registry->template->show('admin_odg_visestruki_jedan');

              else
                  $this->registry->template->show('admin_odg_visestruki_vise');

              exit;
            }
        }
      }

      if($broj_odgovora !== $broj_tocnih)
      {
        $netocni = $_POST['netocan_odgovor'];
        foreach ($netocni as $netocan)
        {
          $id_odgovora = (int)$k->max_id_odgovora();
          $id_odgovora +=1;
          $k->stvori_novi_odgovor($id_odgovora, $id_pitanja, $netocan, "0");
        }
      }

      foreach ($tocni as $tocan)
      {
        $id_odgovora = (int)$k->max_id_odgovora();
        $id_odgovora +=1;
        $k->stvori_novi_odgovor($id_odgovora, $id_pitanja, $tocan, "1");
      }

      $this->registry->template->title = 'Stvaranje pitanja';
      $this->registry->template->show('admin_novo_pitanje');

    }
  }
}

 ?>
