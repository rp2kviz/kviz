<?php


class kvizController extends BaseController
{
	public function index()
	{
		$kviz= new Service();
		$this->registry->template->title = 'Kviz';
		$this->registry->template->lista_kvizova=$kviz->svi_kvizovi();
		$this->registry->template->show( 'kviz_index' );

	}

	public function prikazi_pitanja()
	{
		$kviz= new Service();

	//e ovako, pitanja ćemo prikazati ako smo na stranici kviz_pocetna pritisnuli gumb sa id-em kviza

		if( isset( $_POST["id_kviza"] ) )
				$id = substr( $_POST["id_kviza"], 8 );
		else if( isset( $_SESSION["id_kviza"] ) )
				$id = $_SESSION["id_kviza"];


			else
			{// preusmjeri na početnu.
			header( 'Location: ' . __SITE_URL . '/index.php?rt=kviz' );

			exit;
			}
//u svakom slucaju, ovo sto sam dobila sada ide u session, jer njega promatramo
		$_SESSION['id_kviza'] = $id;
		// Dohvati sva njegova pitanja i odgovore
		$this->registry->template->id_kviza = $id;
		$this->registry->template->popis_pitanja = $kviz->pitanja_iz_kviza($id);
		$this->registry->template->popis_odgovora = $kviz->odgovori_za_kviz($id);
		$this->registry->template->title = 'Kviz';
    $this->registry->template->show( 'pitanja' );
	}

//ako želimo prikazati listu najboljih rezultata, saljemo stranici rezultati_index listu svih kvizova
public function rezultat()
	{
		$kviz= new Service();
		$this->registry->template->lista_kvizova = $kviz->svi_kvizovi();
		$this->registry->template->title = 'High score';
		$this->registry->template->show('rezultati_index');
	}


// prvo spremimo rezultat u bazu, a onda prikazemo rezultate za samo taj kviz, ne sve
public function novi_rez()
{
	$k= new Service();
	if(isset($_POST['korisnicko_ime'])&&isset($_SESSION['id_kviza'])&&isset($_POST['rez']))
	{
		$k->novi_rezultat( $_SESSION['id_kviza'] , $_POST['korisnicko_ime'], $_POST['rez']);


	$this->registry->template->popis_rezultata=$k->svi_rezultati_za_kviz($_SESSION['id_kviza']);
	$this->registry->template->broj_pitanja=$k->broj_pitanja_u_kvizu($_SESSION['id_kviza']);
	$this->registry->template->title = 'Rezultati';
	$this->registry->template->show('kviz_rezultati');
	}

}
//kada korisnik rijesi kviz, racunamo rezultat i prikazemo mu točne odgovore
public function rjeseno()
{
	$k=new Service();
	if( isset( $_POST["id_kviza"] ) )
			$id = substr( $_POST["id_kviza"], 8 );
	else if( isset( $_SESSION["id_kviza"] ) )
			$id = $_SESSION["id_kviza"];
	else
		{// preusmjeri na početnu.
		header( 'Location: ' . __SITE_URL . '/index.php?rt=kviz' );
		exit;
		}
	$_SESSION['id_kviza'] = $id;

//	if(isset($_POST['vidi_rjesenja'])){
		$this->registry->template->id_kviza = $id;
		$this->registry->template->popis_pitanja =$k->pitanja_iz_kviza($id);
		$this->registry->template->popis_odgovora =$k->odgovori_za_kviz($id);
		$this->registry->template->rezultat=$k->izracun_rezultata($id);
		$this->registry->template->title = 'Odgovori';
		$this->registry->template->show('odgovori');
//	}
}


};

?>
