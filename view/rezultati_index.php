<?php require_once 'view/_header.php';
//ispis rezultata svih kvizova, dosegnemo sve kvizove...
$k=new Service();
$lista_kvizova=$k->svi_kvizovi();

foreach( $lista_kvizova as $pomoc)
{//...za svaki kviz izračunaj broj pitanja u kvizu i povuci sve rezultate za taj kviz
	$kviz = new Service();
  $broj_pitanja = $kviz->broj_pitanja_u_kvizu($pomoc->id_kviza );
  $popis_rezultata = $kviz->svi_rezultati_za_kviz($pomoc->id_kviza );
  echo '<h2>' .$pomoc->ime_kviza ;
  echo '</h2>';
  echo '<br>';
  echo '<ol>';
	//ispisat će ih sortano jer sam tako stavila u svi_rezultati_za_kviz
	$br=0;
	foreach( $popis_rezultata as $pomocna )
	   {
			 //nećemo ispisati sve rezultate, nego samo prvih pet
			 if($br<5)
			 		echo '<li>'.$pomocna->ime_korisnika.':'. $pomocna->rezultat.' od '. $broj_pitanja.'</li>';
			$br++;
		}
    echo '</ol><br>';
}
echo "</ul>";

require_once 'view/_footer.php';
?>
