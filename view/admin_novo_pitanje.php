<?php require_once __SITE_PATH . '/view/_header.php'; ?>

<h2>Kviz: <?php echo $naziv_kviza; ?><br>Pitanje br: <?php echo $br_pitanja; ?> </h2>

<p class="message"><?php echo $message; ?></p><br>

  <form method ="post" action="<?php echo __SITE_URL . '/index.php?rt=admin/stvaranje_pitanja'?>">
    Unesite tekst pitanja: <br>
    <textarea name="tekst_pitanja"></textarea> <br><br>
    Odaberite tip odgovora: <br>
    <table>
      <tr>
        <td><input type="radio" name="tip_odgovora" id="tip_odgovora_tekstualni" value="1">Tekstualni</td>
         <td></td>
         <td></td>
      </tr>
      <tr>
        <td><input type="radio" name="tip_odgovora" id="tip_odgovora_jedan" value="2">Višestruki odabir (jedan točan)</td>
        <td> Broj ponuđenih odgovora:
          <select name="broj_odgovora_jedan">
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </td>
        <td></td>
      </tr>
      <tr>
        <td>
          <input type="radio" name="tip_odgovora" id="tip_odgovora_vise" value="3"> Višestruki odabir (više točnih)
        </td>
        <td>Broj ponuđenih odgovora:
          <select name="broj_odgovora_vise">
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </td>
        <td>Broj točnih odgovora:
          <select name="broj_tocnih_odgovora">
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </td>
      </tr>
    </table> <br>
    URL slike za pitanje (Opcionalno):<br>
    <input type="text" name="url_pitanja">

    <button type="submit" name="submit_pitanje">Dodaj pitanje</button>
    <?php
    if($br_pitanja > 3)
    {   ?>
      <button type="submit" name="submit_kraj" formaction="<?php echo __SITE_URL . '/index.php?rt=admin/kraj_kviza'?>">Kraj</button>
      <?php
    }
    ?>
  </form>

<?php require_once __SITE_PATH . '/view/_footer.php'; ?>
