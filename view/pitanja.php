<?php require_once __SITE_PATH . '/view/_header.php';
?>
<div id="pitanja_klasa">
  <form method="post" action="<?php echo __SITE_URL.'/index.php?rt=kviz/rjeseno'?>">
<?php

foreach ($popis_pitanja as $pitanje)
{
  echo "<div> ";
  echo '<p class="ppitanja">'.$pitanje->redni_broj . ". " . $pitanje->tekst_pitanja . '</p><br>';
  if(strlen(trim($pitanje->slika_pitanja))>0)
  {
    ?>
    <img class="slika_pitanja" src="
    <?php echo $pitanje->slika_pitanja?>"><br>
  <?php
  }
  if ($pitanje->tip_pitanja === "1")
  {
    //pitanje nadopuni
    echo 'Odgovor:' ;
    echo '<input type="text" name="pitanje_'.$pitanje->redni_broj.'" ><br>';
  }
  elseif ($pitanje->tip_pitanja==="2")
  {
    // pitanje radio
    $k=new Service();
    $svi_odg=$k->svi_odgovori_za_pitanje($pitanje->id_pitanja);
    foreach ($svi_odg as $odg)
    {
      echo '<input type="radio" name="pitanje_';
      echo $pitanje->redni_broj;
      echo '" value="';
      echo $odg->tekst_odgovora;
      echo '" >';
      echo $odg->tekst_odgovora;
      echo '<br/>';
    }
  }
  elseif ($pitanje->tip_pitanja==="3")
  {
    // pitanje checkbox
    $k=new Service();
    $svi_odg=$k->svi_odgovori_za_pitanje($pitanje->id_pitanja);
    foreach ($svi_odg as $odg)
    {
      echo'<input type="checkbox" name="pitanje_';
      echo $pitanje->redni_broj;
      echo '[]" value="';
      echo $odg->tekst_odgovora;
      echo '">'.$odg->tekst_odgovora.'<br/>';
    }
  }

  echo '</div>';
}

 ?>

</div>
<button id="vidi_rjesenja" class="vidi_rjesenja">Vidi rjesenja!</button>
</form>
<?php require_once __SITE_PATH . '/view/_footer.php'; ?>
