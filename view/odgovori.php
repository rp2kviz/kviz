<?php require_once __SITE_PATH . '/view/_header.php';?>

  <div id="pitanja_klasa">
  <form method="post" action="<?php echo __SITE_URL.'/index.php?rt=kviz/novi_rez'?>">
  <?php

  $k=new Service();

  foreach ($popis_pitanja as $pitanje)
  {
    echo '<p class="ppitanja">'.$pitanje->redni_broj . ". " . $pitanje->tekst_pitanja . '</p>';
    echo "Vaš odgovor: <span style='color:blue;font-weight:bold'>";
    if($pitanje->tip_pitanja!=="3")
      echo $k->ispis_odgovora($pitanje->redni_broj);

    elseif ($pitanje->tip_pitanja==="3")
    {
      if($k->ispis_odgovora($pitanje->redni_broj)!=="Niste unijeli odgovor na ovo pitanje!")
        foreach ( $k->ispis_odgovora($pitanje->redni_broj) as $pit)
        {
          echo $pit;
          echo " ";
        }
      else
      echo "Niste unijeli odgovor na ovo pitanje";
    }

    echo "</span><br/>Točan odgovor: <span style='color:red;font-weight:bold'>";

    foreach ($popis_odgovora as $odg)
    {
      if($odg->tocno_netocno==="1" && $pitanje->id_pitanja===$odg->id_pitanja)
      {
        echo $odg->tekst_odgovora;
        echo " ";
      }
    }
    echo "</span>";
  }
 ?>
 <br/><br/>
<span style='font-size: 20; font-weight:bold'> Vaš rezultat je: <?php echo $rezultat;  ?></span>
<br/>
Ako želite spremiti svoj rezultat, unesite ime:
<input type="text" name="korisnicko_ime" value=""><br/>
<input type="text" class="hide" name="rez" value="<?php echo $rezultat;  ?>"><br/>
<button id="dodaj_rez" class="dodaj_rez">Spremi!</button>
</form>
<?php require_once __SITE_PATH . '/view/_footer.php'; ?>
