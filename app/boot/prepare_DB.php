<?php

require_once '../../model/db.class.php';

$db = DB::getConnection();

try
{
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS kviz (' .
		'id_kviza int NOT NULL PRIMARY KEY,' .
		'ime_kviza varchar(255) NOT NULL,' .
		'slika_path varchar(255) NOT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Kreirana tablica kviz.<br />";


try
{
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS pitanje (' .
		'id_pitanja int NOT NULL PRIMARY KEY,' .
		'id_kviza int NOT NULL,' .
		'tip_pitanja int NOT NULL,' .
		'tekst_pitanja text NOT NULL,' .
		'redni_broj int NOT NULL, ' .
		'slika_pitanja varchar(255) DEFAULT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Kreirana tablica pitanje.<br />";



try
{
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS admin (' .
		'id_admina int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
		'ime_admina varchar(50) NOT NULL,' .
		'pass varchar(255) NOT NULL)'
		);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Kreirana tablica admin.<br />";


try
{
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS odgovor (' .
		'id_odgovora int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
		'id_pitanja int NOT NULL,' .
		'tekst_odgovora text NOT NULL,' .
		'tocno_netocno tinyint NOT NULL)' // {0,1}
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Kreirana tablica odgovor.<br />";


try
{
	$st = $db->prepare(
		'CREATE TABLE IF NOT EXISTS rezultat (' .
		'id_rezultata int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
		'id_kviza int NOT NULL,' .
		'ime_korisnika varchar(40) NOT NULL,' .
		'rezultat int NOT NULL)'
	);

	$st->execute();
}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Kreirana tablica rezultati.<br />";


// Popunjavanje tablica

try
{
    $st = $db->prepare( 'INSERT INTO admin(id_admina, ime_admina, pass) VALUES (:id_admina, :ime_admina, :pass)' );

    $st->execute( array( 'id_admina' => '1', 'ime_admina' => 'admina', 'pass' => password_hash( 'sifraa', PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'id_admina' => '2', 'ime_admina' => 'adminb', 'pass' => password_hash( 'sifrab', PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'id_admina' => '3', 'ime_admina' => 'adminc', 'pass' => password_hash( 'sifrac', PASSWORD_DEFAULT ) ) );


}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

echo "Ubacio korisnike u tablicu admin.<br />";

try
{
	$st = $db->prepare('INSERT INTO kviz(id_kviza, ime_kviza, slika_path) VALUES (:id_kviza, :ime_kviza, :slika_path)');

	$st->execute(array( 'id_kviza'=>'101', 'ime_kviza'=>'Znanost', 'slika_path'=>'http://rp2.studenti.math.hr/~bderic/kviz/images/znanost.jpeg') );
	$st->execute(array( 'id_kviza'=>'102', 'ime_kviza'=>'Sport', 'slika_path'=> 'http://rp2.studenti.math.hr/~bderic/kviz/images/sport.jpg'));
	$st->execute(array( 'id_kviza'=>'103', 'ime_kviza'=>'Filmovi', 'slika_path'=>'http://rp2.studenti.math.hr/~bderic/kviz/images/judy.jpg') );

}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

//pitanja za znanost
try
{
	$st= $db->prepare('INSERT INTO pitanje(id_pitanja, id_kviza, tip_pitanja, tekst_pitanja, redni_broj, slika_pitanja) VALUES (:id_pitanja, :id_kviza, :tip_pitanja, :tekst_pitanja, :redni_broj, :slika_pitanja)' );

	$st->execute( array( 'id_pitanja'=>'1001', 'id_kviza'=>'101','tip_pitanja'=>"2", 'tekst_pitanja' => 'Koji je metal na sobnoj temperaturi u tekućem agregatnom stanju?', 'redni_broj' => '1', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1002', 'id_kviza'=>'101','tip_pitanja'=>"3", 'tekst_pitanja' => 'Odaberite sekundarne boje:', 'redni_broj' => '2', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1003', 'id_kviza'=>'101','tip_pitanja'=>"1", 'tekst_pitanja' => 'Nastavite Fibonaccijev niz: 0,1,1,2,3,5,8,13,21,34,55,89...', 'redni_broj' => '3', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1004', 'id_kviza'=>'101','tip_pitanja'=>"2" ,'tekst_pitanja' => 'Koji ljudski organ proizvodi bilirubin', 'redni_broj' => '4', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1005', 'id_kviza'=>'101','tip_pitanja'=>"2" ,'tekst_pitanja' => 'Gdje se nalazi najmanja kost u ljudskom tijelu?', 'redni_broj' => '5', 'slika_pitanja'=>'') );


	$st->execute( array( 'id_pitanja'=>'1023', 'id_kviza'=>'102','tip_pitanja'=>"1", 'tekst_pitanja' => 'Nastavite citat Muhammad Alija: "Float like a butterfly, sting like a..."', 'redni_broj' => '3', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1021', 'id_kviza'=>'102','tip_pitanja'=>"2", 'tekst_pitanja' => 'Blanka Vlašić je vlasnica koliko olimpijskih medalja?', 'redni_broj' => '1', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1022', 'id_kviza'=>'102','tip_pitanja'=>"3" ,'tekst_pitanja' => 'Na SP 2018, koji su igrači dali golove u utakmici Hrvatska:Argentina?', 'redni_broj' => '2', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1024', 'id_kviza'=>'102','tip_pitanja'=>"2", 'tekst_pitanja' => 'Gdje su održane ljetne Olimpijske igre 1992?', 'redni_broj' => '4', 'slika_pitanja'=>'') );

	$st->execute( array( 'id_pitanja'=>'1031', 'id_kviza'=>'103','tip_pitanja'=>"3", 'tekst_pitanja' => 'Odaberite imena glavnih likova iz filma Klub boraca.', 'redni_broj' => '1', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1032', 'id_kviza'=>'103','tip_pitanja'=>"2" ,'tekst_pitanja' => 'Koji je hrvatski naziv filma Silver Linings Playbook?', 'redni_broj' => '2', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1033', 'id_kviza'=>'103','tip_pitanja'=>"3", 'tekst_pitanja' => 'Odaberite dobitnike Oscara za najbolji film', 'redni_broj' => '3', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1034', 'id_kviza'=>'103','tip_pitanja'=>"2", 'tekst_pitanja' => 'Koji od sljedećih glumaca NIJE glumio Jamesa Bonda', 'redni_broj' => '4', 'slika_pitanja'=>'') );
	$st->execute( array( 'id_pitanja'=>'1035', 'id_kviza'=>'103','tip_pitanja'=>"1" ,'tekst_pitanja' => 'Iz kojeg je filma citat:"Heres looking at you, kid!"', 'redni_broj' => '5', 'slika_pitanja'=>'') );

}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }

try
{
	$st= $db->prepare('INSERT INTO odgovor(id_odgovora, id_pitanja, tekst_odgovora, tocno_netocno) VALUES (:id_odgovora, :id_pitanja, :tekst_odgovora, :tocno_netocno)' );
//kviz1 pitanje1
	$st->execute( array( 'id_odgovora'=>'1101', 'id_pitanja'=>'1001','tekst_odgovora'=>"željezo", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1201', 'id_pitanja'=>'1001','tekst_odgovora'=>"živa" ,'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1301', 'id_pitanja'=>'1001','tekst_odgovora'=>"litij", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1401', 'id_pitanja'=>'1001','tekst_odgovora'=>"magnezij" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1501', 'id_pitanja'=>'1001','tekst_odgovora'=>"kositar", 'tocno_netocno'=>"0") );
//pitanje2
	$st->execute( array( 'id_odgovora'=>'1102', 'id_pitanja'=>'1002','tekst_odgovora'=>"zelena" ,'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1202', 'id_pitanja'=>'1002','tekst_odgovora'=>"crvena", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1302', 'id_pitanja'=>'1002','tekst_odgovora'=>"žuta" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1402', 'id_pitanja'=>'1002','tekst_odgovora'=>"plava" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1502', 'id_pitanja'=>'1002','tekst_odgovora'=>"ljubičasta" ,'tocno_netocno'=>"1") );
//pitanje3
	$st->execute( array( 'id_odgovora'=>'1103', 'id_pitanja'=>'1003','tekst_odgovora'=>"144" ,'tocno_netocno'=>"1") );
//pitanje 4
	$st->execute( array( 'id_odgovora'=>'1104', 'id_pitanja'=>'1004','tekst_odgovora'=>"mozak" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1204', 'id_pitanja'=>'1004','tekst_odgovora'=>"nadbubrežna žlijezda", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1304', 'id_pitanja'=>'1004','tekst_odgovora'=>"jetra", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1404', 'id_pitanja'=>'1004','tekst_odgovora'=>"gušterača" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1504', 'id_pitanja'=>'1004','tekst_odgovora'=>"hipofiza" ,'tocno_netocno'=>"0") );
	//pitanje 5
	$st->execute( array( 'id_odgovora'=>'1105', 'id_pitanja'=>'1005','tekst_odgovora'=>"U nosu" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1205', 'id_pitanja'=>'1005','tekst_odgovora'=>"U uhu", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1305', 'id_pitanja'=>'1005','tekst_odgovora'=>"U šaci", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1405', 'id_pitanja'=>'1005','tekst_odgovora'=>"U stopalu" ,'tocno_netocno'=>"0") );
//kviz sport pitanje 1
	$st->execute( array( 'id_odgovora'=>'1123', 'id_pitanja'=>'1023','tekst_odgovora'=>"bee", 'tocno_netocno'=>"1") );
//pitanje 2
	$st->execute( array( 'id_odgovora'=>'1122', 'id_pitanja'=>'1021','tekst_odgovora'=>"0", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1222', 'id_pitanja'=>'1021','tekst_odgovora'=>"3" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1322', 'id_pitanja'=>'1021','tekst_odgovora'=>"2" ,'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1422', 'id_pitanja'=>'1021','tekst_odgovora'=>"5", 'tocno_netocno'=>"0") );
//pitanje 3
	$st->execute( array( 'id_odgovora'=>'1121', 'id_pitanja'=>'1022','tekst_odgovora'=>"Modrić", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1221', 'id_pitanja'=>'1022','tekst_odgovora'=>"Rebić", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1321', 'id_pitanja'=>'1022','tekst_odgovora'=>"Messi" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1421', 'id_pitanja'=>'1022','tekst_odgovora'=>"Mandžukić" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1521', 'id_pitanja'=>'1022','tekst_odgovora'=>"Rakitić" ,'tocno_netocno'=>"1") );
//pitanje 4
	$st->execute( array( 'id_odgovora'=>'1124', 'id_pitanja'=>'1024','tekst_odgovora'=>"Seul, Južna Koreja" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1224', 'id_pitanja'=>'1024','tekst_odgovora'=>"Nagano, Japan" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1324', 'id_pitanja'=>'1024','tekst_odgovora'=>"Los Angeles, USA" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1424', 'id_pitanja'=>'1024','tekst_odgovora'=>"Barcelona, Španjolska" ,'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1524', 'id_pitanja'=>'1024','tekst_odgovora'=>"London, UK" ,'tocno_netocno'=>"0") );
//kviz filmovi, pitanje 1
	$st->execute( array( 'id_odgovora'=>'1131', 'id_pitanja'=>'1031','tekst_odgovora'=>"Hannah" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1231', 'id_pitanja'=>'1031','tekst_odgovora'=>"Marla", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1331', 'id_pitanja'=>'1031','tekst_odgovora'=>"Joseph", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1431', 'id_pitanja'=>'1031','tekst_odgovora'=>"Tommy", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1531', 'id_pitanja'=>'1031','tekst_odgovora'=>"Taylor" ,'tocno_netocno'=>"1") );
	//2.pitanje
	$st->execute( array( 'id_odgovora'=>'1132', 'id_pitanja'=>'1032','tekst_odgovora'=>"Tužna ljubav", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1232', 'id_pitanja'=>'1032','tekst_odgovora'=>"Srebrna linija", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1332', 'id_pitanja'=>'1032','tekst_odgovora'=>"U dobru i zlu" ,'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1432', 'id_pitanja'=>'1032','tekst_odgovora'=>"Oporavak" ,'tocno_netocno'=>"0") );
	//3.pitanje
	$st->execute( array( 'id_odgovora'=>'1133', 'id_pitanja'=>'1033','tekst_odgovora'=>"Moonlight", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1233', 'id_pitanja'=>'1033','tekst_odgovora'=>"La la land" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1333', 'id_pitanja'=>'1033','tekst_odgovora'=>"The king's speech", 'tocno_netocno'=>"1") );
	$st->execute( array( 'id_odgovora'=>'1433', 'id_pitanja'=>'1033','tekst_odgovora'=>"Midnight in Paris" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1533', 'id_pitanja'=>'1033','tekst_odgovora'=>"Argo" ,'tocno_netocno'=>"1") );
	//4.pitanje
	$st->execute( array( 'id_odgovora'=>'1134', 'id_pitanja'=>'1034','tekst_odgovora'=>"Daniel Craig", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1234', 'id_pitanja'=>'1034','tekst_odgovora'=>"Sean Connery", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1334', 'id_pitanja'=>'1034','tekst_odgovora'=>"George Lazenby" ,'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1434', 'id_pitanja'=>'1034','tekst_odgovora'=>"Timothy Dalton", 'tocno_netocno'=>"0") );
	$st->execute( array( 'id_odgovora'=>'1534', 'id_pitanja'=>'1034','tekst_odgovora'=>"Tom Hiddleston", 'tocno_netocno'=>"1") );
	//5.pitanje
	$st->execute( array( 'id_odgovora'=>'1135', 'id_pitanja'=>'1035','tekst_odgovora'=>"Casablanca" ,'tocno_netocno'=>"1") );


}
catch( PDOException $e ) { exit( "PDO error: " . $e->getMessage() ); }


?>
