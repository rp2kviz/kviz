<?php

class Pitanje
{
	protected $id_pitanja, $id_kviza, $tip_pitanja, $tekst_pitanja, $redni_broj, $slika_pitanja;

	function __construct($id_pitanja, $id_kviza, $tip_pitanja, $tekst_pitanja, $redni_broj, $slika_pitanja)
	{
		$this->id_pitanja = $id_pitanja;
		$this->id_kviza = $id_kviza;
		$this->tip_pitanja = $tip_pitanja;
		$this->tekst_pitanja = $tekst_pitanja;
    $this->redni_broj = $redni_broj;
		$this->slika_pitanja = $slika_pitanja;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }

}


?>
