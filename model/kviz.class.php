<?php

class Kviz
{
	protected $id_kviza, $ime_kviza, $slika_path;

	function __construct( $id_kviza, $ime_kviza, $slika_path)
	{
		$this->id_kviza = $id_kviza;
		$this->ime_kviza = $ime_kviza;
		$this->slika_path = $slika_path;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }


}
?>
