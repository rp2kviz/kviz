<?php
//ovdje dodam sve druge koje mi trebaju
require_once 'db.class.php';
require_once 'kviz.class.php';
require_once 'pitanje.class.php';
require_once 'odgovor.class.php';
require_once 'admin.class.php';
require_once 'rezultat.class.php';


class Service
{
//vraca sve kvizove (id, ime, put do slike)
  function svi_kvizovi()
  {
    try
    {
      $db = DB::getConnection();
      $st = $db->prepare( 'SELECT * FROM kviz' );
      $st->execute();
    }
    catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

    $niz=array();
    while($redak=$st->fetch())
      $niz[]=new Kviz($redak['id_kviza'], $redak['ime_kviza'], $redak['slika_path']);

    return $niz;
  }


//vraca sva pitanja iz kviza s IDjem $id_kviza
  function pitanja_iz_kviza( $id_kviza )
  {
    try
    {
      $db = DB::getConnection();
      $st = $db->prepare( 'SELECT * FROM pitanje WHERE id_kviza=:id_kviza' );
      $st->execute( array( 'id_kviza' => $id_kviza ) );
    }
    catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

    $niz=array();
    while($redak=$st->fetch())
    {
      $niz[]=new Pitanje($redak['id_pitanja'], $redak['id_kviza'], $redak['tip_pitanja'], $redak['tekst_pitanja'], $redak['redni_broj'], $redak['slika_pitanja']);
    }
    return $niz;
  }
//vraca sve odgovore za pitanje s IDjem $id_pitanja
  function svi_odgovori_za_pitanje($id_pitanja)
  {
    try
    {
      $db=DB::getConnection();
      $st=$db->prepare('SELECT * FROM odgovor WHERE id_pitanja=:id_pitanja ');
      $st->execute(array('id_pitanja' => $id_pitanja ));
    }
    catch( PDOException $e){exit('PDO error' .$e->getMessage()); }

    $arr=array();
    while($row=$st->fetch())
    {
      $arr[]=new Odgovor($row['id_odgovora'], $row['id_pitanja'], $row['tekst_odgovora'], $row['tocno_netocno']);
    }
    shuffle($arr);
    return $arr;
  }

// funkcija koja dohvaća sve odgovore za kviz s ID-em $id_kviza
  function odgovori_za_kviz($id_kviza)
  {
    $sva_pitanja = $this->pitanja_iz_kviza($id_kviza);
    $niz = array();

    foreach ($sva_pitanja as $pitanje)
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare( 'SELECT * FROM odgovor WHERE id_pitanja=:id_pitanja' );
        $st->execute( array( 'id_pitanja' => $pitanje->id_pitanja ) );
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      while( $redak = $st->fetch() )
      {
        $niz[] = new Odgovor( $redak['id_odgovora'], $redak['id_pitanja'] , $redak['tekst_odgovora'], $redak['tocno_netocno']);
      }
    }
    return $niz;
  }


// vraca broj pitanja za kviz s IDjem $id_kviza
   function broj_pitanja_u_kvizu($id_kviza)
   {
     try
      {
        $db = DB::getConnection();
        $st = $db->prepare( 'SELECT count(*)FROM pitanje WHERE id_kviza=:id_kviza' );
        $st->execute( array( 'id_kviza' => $id_kviza ) );
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      $num=$st->fetch();
      $n=$num[0];

      return $n;

   }
//vraca rezultate kviza s IDjem $id_kviza u padajucem poretku!!!!
  function svi_rezultati_za_kviz($id_kviza)
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare( 'SELECT * FROM rezultat WHERE id_kviza=:id_kviza ORDER BY rezultat DESC');
        $st->execute( array( 'id_kviza' => $id_kviza ) );
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

      $niz = array();
      while( $redak = $st->fetch() )
      {
        $niz[] = new Rezultat( $redak['id_rezultata'], $redak['id_kviza'], $redak['ime_korisnika'], $redak['rezultat'] );
      }

      return $niz;
    }


// sprema rezultat zavrsenog kviza u bazu
  	function novi_rezultat( $id_kviza, $ime_korisnika, $rezultat )
  	{
  		$db = DB::getConnection();
  		$lista = $this->svi_rezultati_za_kviz( $id_kviza );

  	   $id_rezultata=$this->max_id_rezultata();
       $id_rezultata+=1;
  		try
  		{
  			$st = $db->prepare( 'INSERT INTO rezultat(id_rezultata, id_kviza, ime_korisnika, rezultat) VALUES (:id_rezultata, :id_kviza, :ime_korisnika, :rezultat)' );
  			$st->execute( array( 'id_rezultata' => $id_rezultata, 'id_kviza' => $id_kviza, 'ime_korisnika' => $ime_korisnika, 'rezultat' => $rezultat) );
  		}
  		catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
  	}

//funkcija za dohvat svih admina iz baze
    function dohvati_admine()
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare('SELECT id_admina, ime_admina, pass FROM admin');
        $st->execute();
      }
      catch( PDOException $e) { exit ('PDO error ' . $e->getMessage() );}

      $arr = array();
      while($row = $st->fetch() )
      {
        $arr[] = new Admin($row['id_admina'], $row['ime_admina'], $row['pass']);
      }

      return $arr;
    }

//funkcija za kreiranje novog kviza s id-jem $id_kviza i imenom $ime_kviza
    function stvori_novi_kviz($id_kviza, $ime_kviza, $slika_path)
    {
      try
      {
        $db=DB::getConnection();
        $st = $db->prepare('INSERT INTO kviz (id_kviza, ime_kviza, slika_path) VALUES (:id_kviza, :ime_kviza, :slika_path)');
        $st->execute(array( 'id_kviza'=>$id_kviza, 'ime_kviza'=>$ime_kviza, 'slika_path'=>$slika_path) );
      }
      catch( PDOException $e) {exit('PDO error' . $e->getMessage() ); }
    }

//funkcija za kreiranje novog pitanja u kvizu s id-jem $id_kviza
    function stvori_novo_pitanje($id_pitanja, $id_kviza, $tip_pitanja, $tekst_pitanja, $redni_broj, $slika_pitanja)
    {
      try
      {
        $db=DB::getConnection();
        $st = $db->prepare('INSERT INTO pitanje (id_pitanja, id_kviza, tip_pitanja, tekst_pitanja, redni_broj, slika_pitanja) VALUES (:id_pitanja, :id_kviza, :tip_pitanja, :tekst_pitanja, :redni_broj, :slika_pitanja)' );
        $st->execute( array( 'id_pitanja'=>$id_pitanja, 'id_kviza'=>$id_kviza,'tip_pitanja'=>$tip_pitanja, 'tekst_pitanja' => $tekst_pitanja, 'redni_broj' => $redni_broj, 'slika_pitanja' => $slika_pitanja) );
      }
      catch( PDOException $e) {exit('PDO error' . $e->getMessage() ); }
    }

    //funkcija za kreiranje novog odgovora za pitanje s id-em $id_pitanja
    function stvori_novi_odgovor($id_odgovora, $id_pitanja, $tekst_odgovora, $tocno_netocno)
    {
      try
      {
        $db=DB::getConnection();
        $st= $db->prepare('INSERT INTO odgovor(id_odgovora, id_pitanja, tekst_odgovora, tocno_netocno) VALUES (:id_odgovora, :id_pitanja, :tekst_odgovora, :tocno_netocno)' );
        $st->execute( array( 'id_odgovora'=>$id_odgovora, 'id_pitanja'=>$id_pitanja,'tekst_odgovora'=>$tekst_odgovora, 'tocno_netocno'=>$tocno_netocno) );
      }
      catch( PDOException $e) {exit('PDO error' . $e->getMessage() ); }
    }

//tražimo maksimalan id_kviza do sad, kako bismo dodali idući novi
    function max_id_kvizova()
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare('SELECT id_kviza FROM kviz');
        $st->execute();
      }
      catch( PDOException $e) { exit ('PDO error ' . $e->getMessage() );}

      $max = 0;
      while($row = $st->fetch() )
      {
        $id_num = (int) $row['id_kviza'];
        if ($id_num > $max) $max = $id_num;
      }
      return $max;
    }

    //tražimo maksimalan id_rezultata do sad, kako bismo dodali idući novi
    function max_id_rezultata()
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare('SELECT id_rezultata FROM rezultat');
        $st->execute();
      }
      catch( PDOException $e) { exit ('PDO error ' . $e->getMessage() );}

      $max = 0;
      while($row = $st->fetch() )
      {
        $id_num = (int) $row['id_rezultata'];
        if ($id_num > $max) $max = $id_num;
      }
      return $max;
    }

//tražimo maksimalan id_pitanja do sad, kako bismo dodali idući novi
    function max_id_pitanja()
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare('SELECT id_pitanja FROM pitanje');
        $st->execute();
      }
      catch( PDOException $e) { exit ('PDO error ' . $e->getMessage() );}

      $max = 0;
      while($row = $st->fetch() )
      {
        $id_num = (int) $row['id_pitanja'];
        if ($id_num > $max) $max = $id_num;
      }
      return $max;
    }

//tražimo maksimalan id_odgovora do sad, kako bismo dodali idući novi
    function max_id_odgovora()
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare('SELECT id_odgovora FROM odgovor');
        $st->execute();
      }
      catch( PDOException $e) { exit ('PDO error ' . $e->getMessage() );}

      $max = 0;
      while($row = $st->fetch() )
      {
        $id_num = (int) $row['id_odgovora'];
        if ($id_num > $max) $max = $id_num;
      }
      return $max;
    }
//ovo je fja samo za checkbox kad moramo pobrojati tocne odgovore unutar checkboxa da bismo provjerili
//da je korisnik zaista unio SVE točne odgovore
    function broj_tocnih($pitanje)
    {
      try
      {
        $db = DB::getConnection();
        $st = $db->prepare( 'SELECT SUM(tocno_netocno) FROM odgovor WHERE id_pitanja=:id_pitanja' );
        $st->execute(array( 'id_pitanja' => $pitanje->id_pitanja ) );
      }
      catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
      $tocni=0;

      $num=$st->fetch();
      $tocni=$num[0];

      return $tocni;
    }
//računanje rezultata kojeg je korisnik ostvario
    function izracun_rezultata($id)
    {
      $rezultat=0;
      $k=new Service();
      $popis_pitanja=$k->pitanja_iz_kviza($id);
      $odgovori=$k->odgovori_za_kviz($id);
      foreach ($popis_pitanja as $pitanje)
      {//prolazimo po pitanja i provjeravamo točnost, za svaki tip provjeravamo posebno
        if($pitanje->tip_pitanja==="1")
        {
          foreach( $odgovori as $odgovor )
         		if ( $pitanje->id_pitanja===$odgovor->id_pitanja)
         		{
              if(isset($_POST["pitanje_$pitanje->redni_broj"])&&strtolower($odgovor->tekst_odgovora)===strtolower($_POST["pitanje_$pitanje->redni_broj"]))
                   $rezultat++;
            }
        }
        elseif ($pitanje->tip_pitanja==="2")
        {
          foreach( $odgovori as $odgovor )
          if ( $pitanje->id_pitanja===$odgovor->id_pitanja)
            {
              if(isset($_POST["pitanje_$pitanje->redni_broj"])&& $odgovor->tocno_netocno==="1" && $_POST["pitanje_$pitanje->redni_broj"]===$odgovor->tekst_odgovora)
                $rezultat++;
            }
        }
        elseif($pitanje->tip_pitanja==="3")
        {
          $tocni=0;
          foreach($odgovori as $odgovor)
          if($pitanje->id_pitanja===$odgovor->id_pitanja)
          {
            if(isset($_POST["pitanje_$pitanje->redni_broj"]))
            foreach ($_POST["pitanje_$pitanje->redni_broj"] as $checkbox)
              {
              //prolazimo po checkiranim odgovorima
              if($odgovor->tocno_netocno==="1" && $checkbox===$odgovor->tekst_odgovora)
                $tocni++;
              }
            if ((integer)$tocni===(integer)$k->broj_tocnih($pitanje))
                $rezultat++;
          }
        }
      }
      return $rezultat;
    }

//pomocna funkcija za ispis odgovora, pretvara ono što je u postu odgovora u odgovarajući string
    function ispis_odgovora($rb)
    {
      if(isset($_POST["pitanje_$rb"]) && $_POST["pitanje_$rb"]!=="")
        return $_POST["pitanje_$rb"];
      else
        return "Niste unijeli odgovor na ovo pitanje!";
    }

};

 ?>
