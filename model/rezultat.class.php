<?php
class Rezultat
{
	protected $id_rezultata, $id_kviza, $ime_korisnika, $rezultat;

	function __construct( $id_rezultata, $id_kviza, $ime_korisnika, $rezultat )
	{
		$this->id_rezultata = $id_rezultata;
		$this->id_kviza = $id_kviza;
		$this->ime_korisnika = $ime_korisnika;
		$this->rezultat = $rezultat;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}

?>
