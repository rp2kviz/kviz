<?php
class Odgovor
{
	protected $id_odgovora, $id_pitanja, $tekst_odgovora, $tocno_netocno;

	function __construct( $id_odgovora, $id_pitanja, $tekst_odgovora, $tocno_netocno )
	{
		$this->id_odgovora = $id_odgovora;
		$this->id_pitanja = $id_pitanja;
		$this->tekst_odgovora = $tekst_odgovora;
		$this->tocno_netocno = $tocno_netocno;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}


?>
