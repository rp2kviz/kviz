<?php
require_once 'db.class.php';

class Admin
{
	protected $id_admina, $ime_admina, $pass;

	function __construct($id_admina, $ime_admina, $pass)
	{
		$this->id_admina = $id_admina;
		$this->ime_admina = $ime_admina;
		$this->pass = $pass;
	}

	function __get( $prop ) { return $this->$prop; }
	function __set( $prop, $val ) { $this->$prop = $val; return $this; }
}
?>
